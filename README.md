## Les monnaies libres, qu'est ce que c'est ?
Des monnaies qui permettent :
- Aux vivants en faisant usage d'être égaux face à la création monétaire (chacun sa part, même les générations futures).
- A qui le veut d'en faire usage sans décision d'élu préalable.
- A chacun d'en vérifier le bon fonctionnement.

### Comment est-ce possible ?
- principes et garanties théoriques
- choix techniques actuels

### Pourquoi est-ce une bonne idée ?
- faciliter les échanges de biens et de services entre être humains
- éviter les inégalités systémiques liées à la monnaie
- éviter les crises monétaires
- **tableau comparatif entre systèmes monétaires**

| Critères    | Troc | Etalon Or | Monnaies Dettes (€,$...) | MCL | SEL | Bitcoin, Ethereum... | Monnaies Libres |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Emprunte écologique de fonctionnement | Idéale | Système informatique, production / logistique billets, miniaire | Système informatique, production / logistique billets | Système informatique, production / logistique billets | Système informatique (au moins à grande échelle) | Système informatique énergivore (course à la puissance) | Système informatique |
| Symétrie spaciale | Oui | Non | Non | Non | Oui* | Non* | Oui |
| Symétrie temporelle | Oui | Non | Non | Non | Non* | Non | Oui |
| Fondante | Oui | Non | Non* | ~* | Non* | Non | Oui |
| Charte déontologique | Non | Non | Non | Oui* | ~* | Non | Non* |
| Passage à l'échelle | Inadapté | Eprouvé | Eprouvé | Locale | Locale | Eprouvé | En cours (fluide en théorie) |
| Gouvernance (Distribution du pouvoir de gouvernance) (à petite échelle) | Citoyen | banquaire centralisé | banquaire centralisé | Citoyen en apparence (assujetti à une monnaie dette et sa gouvernance) | Citoyen | très technocratique et légèrement ploutocratique | très technocratique et légèrement citoyen |
| Gouvernance (à grande échelle) | - | banquaire centralisé | banquaire centralisé | centralisé bureau associatif ( en pratique également assujetti à une monnaie dette et sa gouvernance) | concentration représentative (élu / bureau associatif) à grande échelle | légèrement technocratique et très ploutocratique | légèrement technocratique et très citoyen |
| Dépendance technologique de fonctionnement (à petite échelle) | Non | Non | Non | Non | Non | Oui | Non* |
| Dépendance technologique de fonctionnement (à grande échelle) | - | Oui* | Oui | Oui | Oui | Oui | Oui |
| Achat possible sans technologie | Oui | Oui | Oui | Oui | Oui | Indirectement* | Indirectement* |
| Recule sur le système | > 5000 ans | Usage intensif à partir du XVIII siècle | Usage intensif à partir du XX siècle | Usages épisodiques de longue date | Usages épisodiques de longue date | Utilisé depuis 2009 | Utilisé depuis 2017, expérimentations depuis 2012 |


RML12 :
Il s'agit du rendez-vous bi-annuel des partisants des monnaies libres. Théoriciens, développeurs et passionnés s'y retrouve pour parler de l'avancement de chaque élément de l'écosystème numérique autour de Duniter, pour faire vivre la première monnaie libre ( la June, écrite Ğ1 ) grâce à des ventes aux enchères, restaurations et autres proposés en Ğ1. C'est aussi l'occasion de faire découvrir aux curieux les monnaies libres par des conférences et des jeux, tel que le Ğeconomicus qui permet de simuler différent système monétaire et de ce rendre compte de leur impacte sur l’économie par la pratique.
Pour la 12ème édition, Je souhaiterais élargir orateur et publique aux sociologue et économistes, ainsi qu'a différents experts techniques pour faire s'interconnecter des mondes qui ne peuvent que s'enrichir mutuellement. C'est pourquoi cette 12ème éditions sera sur 9 jours et non plus 4, avec les 2 premiers jours, destiné au grand publique, axé sur la découverte des alternatives monétaire et leur comparaison avec les monnaies libres. Suivi d'une journée technique sur les différentes implémentations de cryptomonnaies et leurs écosystèmes. Suivi de deux jours de forum ouvert pour que les rencontres et expérimentations qui devait arriver puisse se faire, Suivi des deux journées développeurs sur les monnaies libres, l'une sur le coeur de duniter et son protocole et sa sécurité, l'autre sur tout l'écosystème qui le rend utilisable par des non techniciens. Enfin les journée grand publique sur les monnaie libre, avec jeux, conférences et place de marché en Ğ1 pour clôturer l'évènement.



# RML 12
- Site accessible à l'adresse : https://rml12.duniter.io/
- Urls de secours : https://rml12.1000i100.fr/
- Urls de secours : http://1000i100.frama.io/rml12/

## Les monnaies libres, qu'est ce que c'est ?
Ce sont des monnaies visant à n'avantager ni ne désavantager aucun des humains vivants en faisant usage.

Comment ?
- principes et garanties théoriques
- choix techniques actuels
        (pour rendre cela possible d'initiative citoyenne)

Pourquoi ? (Pourquoi est-ce une bonne idée de vouloir utiliser des monnaies libres ?)
- faciliter les échanges de biens est de services entre être humains
- stabiliser l'usage et l'accès à la monnaie de génération en génération. (et ainsi éviter 
    - les innégalités systémique liées à la monnaie
    - les crises monétaires
)
- valoriser la valeur intrinsèque de l'être humain (en le rendant, de son vivant co-créateur de monnaie du simple fait d'exister)
    - quel est l'axiome sans quoi la monnaie n'apporte rien : l'humain (la monnaie est un outil pour faciliter les échanges entre être humain)
    - en gardant ça en tête, quel référentiel utiliser pour déterminer comment donner accès à la monnaie ? en étant humain.
    - concrètement -> être humain et vivant est la condition nécessaire et suffisante pour co-créer une part égale de monnaie que ses semblable.
- **tableau comparatif entre systèmes monétaires**

## Ce qu'est une monnaie, par quelqu'un qui sait (Julia)
Definition : Une monnaie c'est un outil d'échange de bien et de services dans une communauté donnée.
Propriété : Plus la communauté qui utilise une monnaie est grande, plus cette monnaie est utilisable et stable.
Et ben, c'est tout ! *rires*

## Présentation brève des rencontres
Axe de progression : simplification 
Sur la base des travaux de Stéphane Laborde sur la Théorie Relative de la Monnaie (TRM),

Je vous propose de participer à l'organisation de la 12ième édition des Rencontres des Monnaies Libres (RML) à Bordeaux en novembre 2018.

La monnaie libre, qu'est ce que c'est ?

C'est une initiative citoyenne visant à permettre la réapropriation de la monnaie de maniere vertueuse.
En maitrisant chacun des aspects de la monnaie, nous endiguons les aléas économiques du système classique.

Technologiquement, c'est au moyen d'une blockchain dont les regles de gestion sont connues et gérés par tous les acteurs sur une base égalitaire.
Cette maitrise s'appuie sur deux concepts : le dividende universel et la toile de confiance.
    
- sujet
  En m'inspirant des travaux de Stéphane Laborde sur la théorie relative de la monnaie, en tant que citoyen, je souhaite pouvoir utiliser une monnaie :
    - dont je connais et peut vérifier les règles de fonctionnement -> liens logiciel libre
    - utilisée principalement pour faire des échanges de biens et services entre êtres humain -> lien monnaie d'échange et de confiance != l'argent pour l'argent
    - égalitaire entre êtres humain d'ici et d'ailleurs, d'aujourd'hui ou des générations futures, pour éviter les crises systémique. -> lien principe de symétrie monnaie libre
    - qui réduit l'impact des grosses fortunes et de l'héritage en étant mécaniquement redistributive (et donc en réduisant les inégalités). -> explication fonte, convergeance
    - dans laquelle je puisse avoir confiance car son fonctionnement ne peut pas être modifié par un petit groupe de puissants. -> décentralsié/blockchain
    - dès maintenant, sans attendre qu'un élu ai le courage politique de la mettre en place. -> initiative citoyenne, adoption volontaire, reposant sur des outils technique (Duniter, Cesium...)
    - qui pour atteindre les buts précédents, me permet d'être crédité chaque jour, en tant qu'être humain, d'une part de cette monnaie, tel un revenu de base inconditionnel ou revenu d'existence non stigmatisant, mais sans vocation à être suffisant pour en vivre dignement. -> DU RdB, similarité et différences
  Cette monnaie existe aujourd'hui, et nous organisons tout les 6 mois des Rencontres des Monnaies Libre (RML) pour la faire découvrir, et être de plus en plus nombreux à l'utiliser et à contribuer à son bon fonctionnement.
  
  En novembre 2018, je compte sur Bordeaux Métropole pour accueillir l'évènement, et j'ai besoin de vous pour m'aider à rendre cela possible.
  
- déroulement
  - Samedi, RML-extra, journée de découverte et sensibilisation autour des monnaies libres, 80 personnes
  - Dimanche, RML-extra, journée non tech sur les systèmes de monnaies non libre, leurs innovations et lacunes, 80 personnes.
  - Lundi, RML-extra, journée tech sur les enjeux technique des écosystème des crypto, 40 personnes
  - Mardi, RML-extra, forum-ouvert, 40 personnes
  - Mercredi, RML-extra, forum-ouvert, 40 personnes
  - Jeudi, RML-classique, journée développeur, 40 personnes
  - Vendredi, RML-classique, journée développeur, 40 personnes
  - Samedi, RML-classique, journée grand publique, 80 personnes
  - Dimanche, RML-classique, journée grand publique, 80 personnes
- Quand
- Où

## Besoins
- Locaux (avec un bonne connexion, pour accueillir jusqu'a 80 personnes, vous pouvez proposer un lieu pour un ou plusieurs jours)
- Repas
  - payable en G1 si possible
  - bio local et/ou récup de fin de marché si possible
  - un menu végan sans gluten si possible (mais pas forcément que ça)
- hébergement
  - chez l'habitant, gratuit, ou à prix libre si possible en G1
- bénévoles
- table chaises
- vidéo projecteur
- materiel de captation et retransmition (et staff pour l'utiliser)
- materiel de sonorisation
- Finances
  - provision de trésorerie (tout budget restant sera restitué aux donnateurs)
  - soutiens à l'organisateur (me dire merci, avec des G1)
  - soutiens au lieux d'accueils
  - soutiens au restorateur
  
  
  
  
# Les autres sites des RML :
- http://rml.creationmonetaire.info/
en particulier (pour les inspirations design) :
  - https://www.monnaielibreoccitanie.org/blog/
  - http://rml9-lehavre.tk/
  - https://rml10.duniter.org/

# article d'explication du sujet
- http://nayya.org/index.php/2017/08/06/une-monnaie-libre-existe/
- http://blog.spyou.org/wordpress-mu/2017/03/14/oh-les-cons-ils-ont-double-hamon/
- https://blog.cgeek.fr/de-linteret-dune-monnaie-libre.html
( bref, la section article du wiki https://duniter.org/fr/wiki/ )
