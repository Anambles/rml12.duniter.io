const now = (new Date).toISOString();
const fs = require("fs");
const md = require('marked');
md.setOptions({headerIds: false, xhtml: true});
replacesInFiles(
    ["index.html","sitemap.xml", "humans.txt"],
    {
        "now":now,
        "programme":fs.readFileSync('generated.programme.html', 'utf8'),
        "money_systems_comparison":fs.readFileSync('template/pages/money_systems_comparison.html', 'utf8'),
	    "symetrie_DU":md(fs.readFileSync('template/pages/symetrie_DU.md', 'utf8')),
	    "adoption_citoyenne":md(fs.readFileSync('template/pages/adoption_citoyenne.md', 'utf8')),
	    "page_libre":md(fs.readFileSync('template/pages/page_libre.md', 'utf8')),
	    "page_theorie":md(fs.readFileSync('template/pages/page_theorie.md', 'utf8')),
	    "page_technique":md(fs.readFileSync('template/pages/page_technique.md', 'utf8')),
	    "faciliter_echanges":md(fs.readFileSync('template/pages/faciliter_echanges.md', 'utf8')),
	    "stop_inegalites_systemiques":md(fs.readFileSync('template/pages/stop_inegalites_systemiques.md', 'utf8')),
	    "stop_crise":md(fs.readFileSync('template/pages/stop_crise.md', 'utf8')),
	    "transition_ecologique_sociale":md(fs.readFileSync('template/pages/transition_ecologique_sociale.md', 'utf8')),
	    "espace_presse":md(fs.readFileSync('template/pages/espace_presse.md', 'utf8')),
    });
function replacesInFiles(fileList,replaces) {
    for(let fileName of fileList) replacesInFile(fileName,replaces);
}
function replacesInFile(fileName,replaces) {
    const regex = new RegExp(Object.keys(replaces).map(key=>`\\{build\\:${key}\\}`).join('|'),'g');
    const replacements = {};
    for (let k in replaces) replacements[`{build:${k}}`]=replaces[k];

    let fileContent = fs.readFileSync(`template/${fileName}`,"utf8");
    fileContent = fileContent.replace(regex,match=>replacements[match]);
    // anchors to SEO links
	fileContent = fileContent.replace(/="#([^"]*)"/g,(match,catched)=>`="./${catched?catched+'.html':''}"`);

    fs.writeFileSync(`generated.public/${fileName}`,fileContent,"utf8");
}
