# Faciliter les échanges

## Monnaie
Les monnaies libres sont des monnaies. En tant que telles, elles facilitent les échanges par rapport au troc ou autres systèmes
non monétisés en fournissant une unité de mesure à granularité fine qui permet d'équilibrer des échanges sinon
disproportionnés.
Les monnaies permettent également de gérer efficacement des échanges différés. Enfin, elles servent
de vecteur de confiance pour leur capacité d'échange contre un bien ou service auprès de tiers, et cela au sein
de communautés de tailles bien supérieures à celles où tout le monde se connait (et où le regard collectif et le besoin
d'appartenance suffisent à gérer les échanges sans besoin de monnaie).

## Répartition
A la différence des monnaies classiques, les monnaies libres sont mécaniquement redistributives.
Leur mode de création monétaire s'assure qu'il n'y ait jamais d'assèchement de l'économie.
Ainsi, les individus constituant l'espace économique d'une monnaie libre ne peuvent que marginalement être à court de monnaie
s'ils consomment nettement plus qu'ils ne fournissent. Ce n'est donc pas le système qui est asséché par la captation de quelques-uns,
mais quelques-uns qui peuvent se retrouver temporairement à vide s'ils n'équilibrent pas leur compte.
Et même à vide, le DU viendra progressivement les remettre en capacité d'échanger.

Toute monnaie libre, de part son mode de création monétaire, est fondante pour les personnes morales (entreprises, associations...)
et autres comptes portefeuilles.
Pour les personnes physiques (individu membre de l'espace économique), elle est convergente :
chaque compte membre, s'il achète autant qu'il vend, va converger vers la moyenne progressivement, grâce au DU.

C'est ce mécanisme qui permet aux monnaies libres de faciliter les échanges au-delà des monnaies classiques,
en répartissant durablement la monnaie entre les individus pour qu'ils restent au coeur d'une économie bien réelle.
Une économie axée sur les échanges entre êtres humains, fluidifiée par l'apport régulier de monnaie par ces derniers.
